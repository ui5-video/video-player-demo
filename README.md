# UI5 video player demo

This is a demo UI5 app showing how to use the [custom UI5 control / library video player](https://gitlab.com/ui5-video/video-player). The app can be run using [UI5 tooling](https://sap.github.io/ui5-tooling/).

Sample movies are taken from [W3 school samples](https://www.w3schools.com/html/html5_video.asp).

## Run app

Clone the repository and make sure you have ui5 tooling installed (npm install --global @ui5/cli).

```sh
npm install
ui5 serve
```

Access the app at http://localhost:8080

The app includes two sample movies. By default, one is already loaded at startup. The other can be loaded by entering movie.mp4 in the input field. Several properties control how the UI5 video element is working

```json
autoplay="false"
showControls="true"
source="path to video"
```

* autoplay controls whether the video is started after it is loaded
* showContorls defines if the built-in video controls - start, stop, pause, fullscreen - are visible or not. In combination with autoplay, the video can be forced to play and the user cannot stop the video
* source is the location from where the browser is loading the video.

## Screenshots

![demo1](./demo1.png)

![demo2](./demo2.png)
