/*
 * (c) Copyright Tobias Hofmann
 * https://www.itsfullofstars.de
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */
sap.ui.define([
  "de/itsfullofstars/VideoDemo/controller/BaseController",
  "sap/ui/model/json/JSONModel",
  "sap/ui/core/Fragment"
], function(Controller, JSONModel, Fragment) {
  "use strict";

  return Controller.extend("de.itsfullofstars.VideoDemo.controller.Home", {

    onInit: function () {
      this._initVideoModel();
    },

    doVideo: function(oEvent) {
      var sValue = "./movies/" + this.byId("inpUrl").getValue();
      console.log(sValue);
      this.getModel("video").setProperty("/Source", sValue);
    },

    /**
     * Opens a dialog to scan barcode codes
     * Uses an XML fragment for the dialog.
     * @public
     */
    openBarcodeDialog: function() {
      this._stopCamera();
      if (!this._oBarcodeDialog) {
        sap.ui.core.Fragment.load({
          id: "scanBarcode",
          name: "de.itsfullofstars.BarcodeScannerDemo.view.fragments.VideoDialog",
          controller: this
        }).then(function (oDialog) {
          this._oBarcodeDialog = oDialog;
          this._oBarcodeDialog.setModel(this.getView().getModel("video"));
          this._oBarcodeDialog.open();
        }.bind(this));
      } else {
        this._oBarcodeDialog.open();
      }
    },

    /**
		 * Close dialog used to scan a barcode code
		 * @public
		 */
		onBarcodeDialogClose: function (pressEvent) {
      // get fragment with barcode scanner control and stop the camera
			var oScanner = Fragment.byId("scanBarcode", "barcodescanner");
      oScanner.stopCamera();
      // close the dialog
			this._oBarcodeDialog.close();
			this._oBarcodeDialog.destroy(true);
			this._oBarcodeDialog = undefined;
		},
    
    /**
     * Event handler for the scanned event of the UI control barcode scanner.
     * The scanned text from the barcode code can be retrieved from the value parameter of the event.
     * This method will make the scanned barcode code text available in the model barcode in the property Barcode.
     * @param {*} oEvent 
     * @public
     */
    onBarcodeScanned: function(oEvent){
			var sValue = oEvent.getParameter("value");
      this.getView().getModel("video").setProperty("/Barcode", sValue); 
    },

    /**
     * Resets the barcode code value in the model barcode.
     * Allows to re-scan a barcode code and see the result.
     * @public
     */
    clearBarcode: function() {
      this._initBarcodeModel();
    },

    /**
     * Closes the QR code camera capture
     * Allows to see how to release the camera after a barcode code is scanned and validated
     * @public
     */
    closeBarcode: function() {
      this._stopCamera();
    },

    /**
     * 
     * Internal methods
     * 
    **/

    /**
     * Stops the camera access of the barcode code control.
     * @private
     */
    _stopVideo: function() {
      var oScanner = this.byId("barcodescanner");
			oScanner.stopCamera();
    },

    /**
     * Initialize the barcode code model
     * @private
     */
    _initVideoModel: function() {
      var oModel = new JSONModel(
        {
          Source: "./movies/mov_bbb.mp4",
          Type: "video/mp4"
        });
			this.getView().setModel(oModel, "video");
    }

  });
});
